import pycurl
from io import BytesIO 

def curl_operation(url="pastebin.php",post=""):
  b_obj = BytesIO() 
  crl = pycurl.Curl() 
  crl.setopt(crl.URL, 'https://paledega.sourceforge.io/'+url)
  crl.setopt(crl.POSTFIELDS, post.encode('utf-8'))
  crl.setopt(crl.WRITEDATA, b_obj)
  crl.perform() 
  crl.close()
  get_body = b_obj.getvalue()
  return get_body.decode('utf8')

