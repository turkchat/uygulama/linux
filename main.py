from pastebin import *
from view import *
from gi.repository import Gtk

w=Gtk.Window()
w.set_size_request(400,300)
e=Gtk.TextView()
b=Gtk.Button("Paste")
l=Gtk.Entry()
box=Gtk.VBox()

def p(w):
  textbuffer=e.get_buffer()
  start_iter = textbuffer.get_start_iter()
  end_iter = textbuffer.get_end_iter()
  text = textbuffer.get_text(start_iter, end_iter, True)   
  print(text)
  aa=paste(str(text))
  l.set_text("https://paledega.sourceforge.io/view.php?plain&paste="+aa)
  print(view(aa))

Gtk.init()
w.connect("destroy",Gtk.main_quit)
w.add(box)
b.connect("clicked",p)
box.pack_start(e,True,True,0)
box.pack_end(b,False,False,0)
box.pack_start(l,False,False,0)
w.show_all()
Gtk.main()
